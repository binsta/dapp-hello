#!/bin/sh

prefix=/home/binston/Public/playground/rust/learn/solana/figment/learn-solana-dapp/program/target/debug/build/jemalloc-sys-f9f5161010a27186/out
exec_prefix=/home/binston/Public/playground/rust/learn/solana/figment/learn-solana-dapp/program/target/debug/build/jemalloc-sys-f9f5161010a27186/out
libdir=${exec_prefix}/lib

LD_PRELOAD=${libdir}/libjemalloc.so.2
export LD_PRELOAD
exec "$@"
